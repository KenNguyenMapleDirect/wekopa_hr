<!doctype html>
<html dir="ltr" lang="en" data-lang="" data-template="home">
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/vendor.css">
    <link rel="stylesheet" href="/css/main.css">

    <title>We-Ko-Pa Casino Resort - Fort McDowell AZ Casino Resort</title>
    <meta name="description"
          content="Step out of the desert and into the oasis that is We-Ko-Pa Casino Resort in Fort McDowell, AZ. Experience our casino, dining, spa, golf, & so much more."/>
    <meta name="keywords" content=""/>
    <meta property="og:site_name" content="We-Ko-Pa Casino Resort"/>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="robots" content="index, follow"/>
    <meta name="google-site-verification" content="VhlapJrn_gGxP2rmGDKoEadmbjIqRcXjaf_lY12CkMM"/>
    <meta property="og:title" content="We-Ko-Pa Casino Resort - Fort McDowell AZ Casino Resort"/>
    <meta property="og:description"
          content="Step out of the desert and into the oasis that is We-Ko-Pa Casino Resort in Fort McDowell, AZ. Experience our casino, dining, spa, golf, & so much more."/>
    <meta property="og:locale" content="en_US"/>

</head>
<body class="g_home body--header1">

<a class="ada-skip" tabindex="0" href="#main">Skip to main content</a>
<header class="site-header">
    <a class="brand-logo js-galaxy-logo" tabindex="0" href="https://www.wekopacasinoresort.com/">
        <img src="https://cdn.galaxy.tf/thumb/sizeW500/uploads/s/cms_image/001/595/697/1595697462_5f1c6936aaa68-thumb.svg"
             alt="We-Ko-Pa Casino Resort Logo"/>
    </a>
    <div class="secondary-navigation">
        <div class="language-switcher">
        </div>
        <ul class="secondary-navigation-list">
            <li class="secondary-navigation-item">
                <a class="nav-item" href="player-sign-up.html">
                    HR Sign-Up
                </a>
            </li>
            <li class="secondary-navigation-item">
                <a class="nav-item" href="/admin" target="_blank">
                    HR Sign-In
                </a>
            </li>
        </ul>
    </div>
    <nav class="main-navigation">
        <ul class="level-one">
            <li class="has-submenu" >
                <a href="/search-jobs" class="">
                    <span>Search Jobs</span>
                </a>
            </li>
            <li class="has-submenu" >
                <a href="/" class="">
                    <span>View All Jobs</span>
                </a>
            </li>
            <li class="has-submenu" >
                <a href="/help" class="">
                    <span>Help</span>
                </a>
            </li>
        </ul>
    </nav>
    <a class="book-direct btn_action tablet-book-direct" href="https://reservations.travelclick.com/96432"
       target="_blank">book
        today</a>
    <a tabindex="0" class="mobile-menu-link" href="resort/packages.html">Promotions</a>
    <div class="close-txt"><span class="menu-txt">Menu</span><span class="close-menu-txt">Close Menu</span></div>
    <button class="btn-menu" data-toggle="modal" data-target="#menu-mobile">
        <span class="bar1"></span>
        <span class="bar2"></span>
        <span class="bar3"></span>
    </button>
    <div class="menu-mobile">
        <!-- This is an example to show you how a simple loop for menu works -->
        <nav class="main-navigation">
            <ul class="level-one">
                <li class="has-submenu" >
                    <a
                        href="#" class=" level-one-link">
                        <span>Search Jobs</span>
                    </a>
                </li>
                <li class="has-submenu" >
                    <a
                        href="#" class=" level-one-link">
                        <span>View All Jobs</span>
                    </a>
                </li>
                <li class="has-submenu" >
                    <a
                        href="#" class=" level-one-link">
                        <span>Help</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</header>
<main id="main" class="main">
    <div class="booking-mask ">
        <form class="booking-form" action="https://reservations.travelclick.com/96432" method="get" target="_blank"
              autocomplete="off">
            <div class="row">
                <!-- todo List all jobs -->
            </div>
        </form>
    </div>
    <section class="hero-home main-visual-wrp ">
        <div class="hero-container set-slick" data-slick='{
               "lazyLoad": "ondemand",
               "autoplay": true,
               "arrows": false,
               "appendDots": ".hero-home .slide-dots .dot-wrp",
               "dots":true,
               "fade": true,
               "autoplaySpeed": 5000,
               "speed": 500,
               "touchThreshold": 10,
               "infinite":true
               }'>
            <div class="slide youtube">
                <div class="video-wrap ratio-content" aria-hidden="true">
                    <div class='video-background'>
                        <div data-id="tvs-id-a09238435be2e862343811929658cb9a" class='video-foreground is-tvs'>

                            <video autoplay loop id="vid" muted>
                                <source src="./videos/video-1608185609.mp4" type="video/mp4">
                            </video>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="slide-dots">
            <div class="dot-wrp"></div>
            <span class="btnPlay"></span>
        </div>
    </section>
</main>
<footer>
    <nav class="footer_nav footer-top-nav js-galaxy-footer"><a class="nav-item footer-top-menu-item" href="https://www.wekopacasinoresort.com/">About
            We-Ko-Pa</a>
        <a class="nav-item footer-top-menu-item" href="contact-directions.html">Contact & Directions</a>
        <a class="nav-item footer-top-menu-item" href="newsletter.html">Newsletter</a>
        <a class="nav-item footer-top-menu-item" href="news-press.html">News & Press</a>
        <a class="nav-item footer-top-menu-item" href="faq.html">FAQ</a>
    </nav>
    <div class="footer-middle-content">
        <div class="col-md-3 col-sm-6 px-0 footer-address-section js-galaxy-footer">
            <h3 tabindex="0" class="footer-subtitle">We-Ko-Pa Casino Resort</h3>
            <div tabindex="0" class="footer-address">
                <p>10438 WeKoPa Way<br/>
                    Fort McDowell, AZ 85264
                </p>
            </div>
            <p>Phone: <a class="click-to-call" href="tel: 480-789-4957">480-789-4957</a></p>
            <p>Toll Free: <a class="click-to-call" href="tel:855-957-9467">855-957-9467</a></p>
        </div>
        <div class="mobile-only">
            <nav class="footer_nav footer-top-nav js-galaxy-footer"><a class="nav-item footer-top-menu-item"
                                                                       href="about.html">About We-Ko-Pa</a>
                <a class="nav-item footer-top-menu-item" href="contact-directions.html">Contact & Directions</a>
                <a class="nav-item footer-top-menu-item" href="newsletter.html">Newsletter</a>
                <a class="nav-item footer-top-menu-item" href="news-press.html">News & Press</a>
                <a class="nav-item footer-top-menu-item" href="faq.html">FAQ</a>
            </nav>
            <nav class="footer_nav footer-bottom-nav js-galaxy-footer"><a class="nav-item footer-bottom-menu-item"
                                                                          href="privacy-policy.html">Privacy Policy</a>
                <a class="nav-item footer-bottom-menu-item" href="notice-of-accessibility.html">Notice of
                    Accessibility</a>
                <a class="nav-item footer-bottom-menu-item"
                   href="https://www.ziprecruiter.com/c/Fort-McDowell-Wekopa-Resort-Casino/Jobs" target="_blank">Wekopa
                    Casino Resort Career</a>
                <a class="nav-item footer-bottom-menu-item"
                   href="https://www.indeed.com/cmp/Fort-Mcdowell-Enterprises-2" target="_blank">Enterprises Career</a>
            </nav>
        </div>
        <div class="col-md-6 col-sm-6 px-0 footer-email-offers">
            <h3 tabindex="0" class="footer-subtitle">Email Offers</h3>
            <p>Get special offers and news delivered right to your<br/>
                email, Sign-up today!
            </p>
            <form class="newletter-box" method="get" action="https://www.wekopacasinoresort.com/newsletter">
                <input class="newsletter-input" type="text" placeholder="email address" name="email"/>
                <button type="submit" class="btn_action"><span>sign-up</span></button>
            </form>
        </div>
        <div class="col-md-3 col-sm-12 pl-0 footer-social-area js-galaxy-footer">
            <h3 tabindex="0" class="footer-subtitle">Stay Social</h3>
            <a class="smo-link facebook" href="https://www.facebook.com/WeKoPaResort" target="_blank" rel="noopener"
               aria-label="facebook (opens in a new tab)"></a>
            <a class="smo-link twitter" href="https://twitter.com/WeKoPaResort" target="_blank" rel="noopener"
               aria-label="twitter (opens in a new tab)"></a>
            <a class="smo-link instagram" href="https://www.instagram.com/wekoparesort/?hl=en" target="_blank"
               rel="noopener" aria-label="instagram (opens in a new tab)"></a>
            <a class="smo-link youtube" href="https://www.youtube.com/channel/UCjN5MZBalIj_7s2F9zYLGmQ" target="_blank"
               rel="noopener" aria-label="youtube (opens in a new tab)"></a>
            <a class="smo-link linkedIn" href="https://www.linkedin.com/company/wekopa-resort-&amp;-conference-center"
               target="_blank" rel="noopener" aria-label="linkedIn (opens in a new tab)"></a>
            <a class="smo-link tripadvisor"
               href="https://www.tripadvisor.com/Hotel_Review-g9598904-d575854-Reviews-We_Ko_Pa_Casino_Resort-Fort_McDowell_Arizona.html"
               target="_blank" rel="noopener" aria-label="tripadvisor (opens in a new tab)"></a>
        </div>
    </div>
    <nav class="footer_nav footer-bottom-nav js-galaxy-footer"><a class="nav-item footer-bottom-menu-item"
                                                                  href="privacy-policy.html">Privacy Policy</a>
        <a class="nav-item footer-bottom-menu-item" href="notice-of-accessibility.html">Notice of Accessibility</a>
        <a class="nav-item footer-bottom-menu-item"
           href="https://www.ziprecruiter.com/c/Fort-McDowell-Wekopa-Resort-Casino/Jobs" target="_blank">Wekopa Casino
            Resort Career</a>
        <a class="nav-item footer-bottom-menu-item" href="https://www.indeed.com/cmp/Fort-Mcdowell-Enterprises-2"
           target="_blank">Enterprises Career</a>
    </nav>
    <div class="footer-logos js-galaxy-footer"><a class="footer-logo bg golf_club" href="https://wekopa.com/"
                                                  tabindex="0" aria-label="Wekopa Golf Club"
                                                  target="_blank"></a>
        <a class="footer-logo bg eagle_view_resort" href="https://www.eagleviewrvresort.com/" tabindex="0"
           aria-label="Eagle View RV Resort"
        ></a>
        <a class="footer-logo bg mcDowell_adventures" href="http://www.fortmcdowelladventures.com/" tabindex="0"
           aria-label="Fort McDowell Adventures"
           target="_blank"></a>
    </div>
    <a class="book-direct btn_action mobile-book-direct" href="https://reservations.travelclick.com/96432"
       target="_blank">book
        today</a>
</footer>
<script src="https://maps.googleapis.com/maps/api/js?v=3"></script>
<script src="/js/pikaday5f4e.js" data-cookieconsent="ignore" type="text/javascript"></script>
<script src="/js/pikarange5f4e.js" data-cookieconsent="ignore" type="text/javascript"></script>
<script src="/js//bundle5f4e.js"></script>
</body>
</html>
